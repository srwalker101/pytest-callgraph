if exists("g:loaded_pycallgraph")
    finish
endif
let g:loaded_pycallgraph = 1

function! s:extract_function_name(line) abort
    return matchlist(a:line, '\v\s*def\s+([^(]+).*$')[1]
endfunction

function! s:get_nearest_function() abort
    let line_no = line(".")
    let found = 0
    while line_no > 0
        let line = getline(line_no)
        if match(line, "def ") != -1
            let found = 1
            break
        endif

        let line_no = line_no - 1
    endwhile

    if !found
        return
    endif

    " get the function name
    let line = getline(line_no)
    return line
endfunction

function! s:build_absolute_path() abort
    return expand("%:p")
endfunction

function! s:get_callers() abort
    let line = s:get_nearest_function()
    let fn_name = s:extract_function_name(line)
    let file_path = s:build_absolute_path()
    return {'path': file_path, 'function': fn_name}
endfunction

function! s:quickfix_callers() abort
    let callers = s:get_callers()
    let cmd = "gen-pycallgraph --function " . callers['function'] . " --filename " . callers['path']
    let old_errorformat = &errorformat
    set errorformat=%f:%l
    cgetexpr system(cmd)
    let &errorformat = old_errorformat
    copen
endfunction

command! Callers call <SID>quickfix_callers()

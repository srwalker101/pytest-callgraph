#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import codecs
from setuptools import setup


def read(fname):
    file_path = os.path.join(os.path.dirname(__file__), fname)
    return codecs.open(file_path, encoding="utf-8").read()


setup(
    name="pytest-callgraph",
    version="0.1.0",
    author="Simon Walker",
    author_email="s.r.walker101@googlemail.com",
    maintainer="Simon Walker",
    maintainer_email="s.r.walker101@googlemail.com",
    license="Apache Software License 2.0",
    url="https://github.com/mindriot101/pytest-callgraph",
    description="Generate callgraphs from pytest",
    long_description=read("README.md"),
    py_modules=["pytest_callgraph"],
    python_requires=">=3.5",
    install_requires=["pytest>=3.5.0"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Framework :: Pytest",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Testing",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: Apache Software License",
    ],
    entry_points={
        "pytest11": [
            "callgraph = pytest_callgraph",
        ],
        "console_scripts": [
            "gen-pycallgraph = pytest_callgraph:cli",
        ],
    },
)

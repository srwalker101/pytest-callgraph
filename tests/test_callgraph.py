# -*- coding: utf-8 -*-

import sqlite3

import pytest

from pytest_callgraph import quux, Database, foo


def test_query():
    db = Database("scratch/callgraph.sqlite")
    rows = list(db.query("bar", "/home/simon/dev/pytest-callgraph/pytest_callgraph.py"))
    assert len(rows) == 1
    assert rows[0] == {
        "func_name": "bar",
        "caller_filename": "/home/simon/dev/pytest-callgraph/pytest_callgraph.py",
        "caller_name": "quux",
        "caller_line_no": 18,
        "func_line_no": 13,
        "caller_func_line_no": 17,
    }


def test_foo():
    assert quux() == 10
    assert foo() == 10


@pytest.mark.skip
def test_bar_fixture(testdir):
    """Make sure that pytest accepts our fixture."""

    # create a temporary pytest test module
    testdir.makepyfile(
        """
        def test_sth(bar):
            assert bar == "europython2015"
    """
    )

    # run pytest with the following cmd args
    result = testdir.runpytest("-v")

    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        [
            "*::test_sth PASSED*",
        ]
    )

    # make sure that that we get a '0' exit code for the testsuite
    assert result.ret == 0


@pytest.mark.skip
def test_help_message(testdir):
    result = testdir.runpytest(
        "--help",
    )
    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        [
            "callgraph:",
            '*--foo=DEST_FOO*Set the value for the fixture "bar".',
        ]
    )


@pytest.mark.skip
def test_hello_ini_setting(testdir):
    testdir.makeini(
        """
        [pytest]
        HELLO = world
    """
    )

    testdir.makepyfile(
        """
        import pytest

        @pytest.fixture
        def hello(request):
            return request.config.getini('HELLO')

        def test_hello_world(hello):
            assert hello == 'world'
    """
    )

    result = testdir.runpytest("-v")

    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        [
            "*::test_hello_world PASSED*",
        ]
    )

    # make sure that that we get a '0' exit code for the testsuite
    assert result.ret == 0

# -*- coding: utf-8 -*-

import pytest
import sys
import sqlite3


def foo():
    return 10


def bar():
    return foo()


def quux():
    return bar()


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class Database:
    def __init__(self, dbname):
        self.conn = sqlite3.connect(dbname)
        self.conn.row_factory = dict_factory
        self.cursor = self.conn.cursor()
        self.seen = set()

    def query(self, function_name, function_filename):
        self.cursor.execute(
            """
            SELECT
                tracepoints.func_name,
                caller_filename,
                caller_name,
                caller_line_no,
                tracepoints.func_line_no,
                site.func_line_no AS caller_func_line_no
            FROM tracepoints
            JOIN (
                SELECT func_name as site_func_name, func_line_no
                FROM tracepoints
            ) AS site
            ON tracepoints.caller_name = site.site_func_name
            WHERE func_name = ?
            AND event_type = "call"
            AND func_filename = ?
        """,
            (function_name, function_filename),
        )

        yield from self.cursor

    def init_db(self):
        with self.conn as conn:
            cursor = conn.cursor()
            cursor.execute("DROP TABLE IF EXISTS tracepoints")
            cursor.execute(
                """
            CREATE TABLE tracepoints (
                id integer not null primary key,
                event_type string,
                func_name string,
                func_filename string,
                func_line_no integer,
                caller_name string,
                caller_filename string,
                caller_line_no integer
                )
                """
            )
            cursor.execute(
                """
            CREATE INDEX idx_func_name ON tracepoints (func_name)
                """
            )

    def insert(
        self,
        event_type,
        func_name,
        func_filename,
        func_line_no,
        caller_name,
        caller_filename,
        caller_line_no,
    ):
        seen_key = (
            func_name,
            func_filename,
            func_line_no,
            caller_name,
            caller_filename,
            caller_line_no,
        )
        if seen_key in self.seen:
            return

        self.cursor.execute(
            """
        INSERT INTO tracepoints (
        event_type,
        func_name,
        func_filename,
        func_line_no,
        caller_name,
        caller_filename,
        caller_line_no
        ) VALUES (?, ?, ?, ?, ?, ?, ?)
        """,
            (
                event_type,
                func_name,
                func_filename,
                func_line_no,
                caller_name,
                caller_filename,
                caller_line_no,
            ),
        )
        self.seen.add(seen_key)

    def commit(self):
        self.conn.commit()


class TracingProfiler:

    EVENTS = {"call"}

    def __init__(self):
        self.db = Database("callgraph.sqlite")
        self.db.init_db()

    def pytest_runtest_call(self, item):
        sys.setprofile(self.tracer)

    def pytest_runtest_teardown(self, item, nextitem):
        sys.setprofile(None)

    def pytest_terminal_summary(self, terminalreporter):
        terminalreporter.write("\nCallgraph captured\n")

    def pytest_sessionfinish(self, session, exitstatus):
        self.db.commit()

    def tracer(self, frame, event_type, arg):
        if event_type not in self.EVENTS:
            return

        self.db.insert(
            event_type=event_type,
            func_name=frame.f_code.co_name,
            func_filename=frame.f_code.co_filename,
            func_line_no=frame.f_lineno,
            caller_name=frame.f_back.f_code.co_name,
            caller_filename=frame.f_back.f_code.co_filename,
            caller_line_no=frame.f_back.f_lineno,
        )


def pytest_addoption(parser):
    group = parser.getgroup("callgraph")
    group.addoption(
        "--gengraph",
        action="store_true",
        default=False,
        help="Generate callgraph for vim navigation.",
    )

    # TODO: add configuration value
    # parser.addini("HELLO", "Dummy pytest.ini setting")


def pytest_configure(config):
    gengraph_enable = config.getvalue("gengraph")
    if gengraph_enable:
        config.pluginmanager.register(TracingProfiler())


# For the CLI
def cli():
    import argparse
    import os

    parser = argparse.ArgumentParser()
    parser.add_argument("--filename", required=True)
    parser.add_argument("--function", required=True)
    args = parser.parse_args()

    search_filename = os.path.realpath(args.filename)

    # TODO: any error handling at all...
    db = Database("callgraph.sqlite")
    sites = list(db.query(args.function, search_filename))

    seen = set()
    for site in sites:
        filename = site["caller_filename"]
        line_no = site["caller_line_no"]
        # De-duplicate the outputs
        if (filename, line_no) in seen:
            continue

        seen.add((filename, line_no))
        print(f"{filename}:{line_no}")

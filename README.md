# pytest-callgraph

[![PyPI version](https://img.shields.io/pypi/v/pytest-callgraph.svg)](https://pypi.org/project/pytest-callgraph)

[![Python versions](https://img.shields.io/pypi/pyversions/pytest-callgraph.svg)](https://pypi.org/project/pytest-callgraph)

[![See Build Status on Travis CI](https://travis-ci.org/mindriot101/pytest-callgraph.svg?branch=master)](https://travis-ci.org/mindriot101/pytest-callgraph)

[![See Build Status on AppVeyor](https://ci.appveyor.com/api/projects/status/github/mindriot101/pytest-callgraph?branch=master)](https://ci.appveyor.com/project/mindriot101/pytest-callgraph/branch/master)

Generate callgraphs from pytest

------------------------------------------------------------------------

This [pytest](https://github.com/pytest-dev/pytest) plugin was generated
with [Cookiecutter](https://github.com/audreyr/cookiecutter) along with
[\@hackebrot](https://github.com/hackebrot)\'s
[cookiecutter-pytest-plugin](https://github.com/pytest-dev/cookiecutter-pytest-plugin)
template.

## Features

-   TODO

## Requirements

-   TODO

## Installation

You can install "pytest-callgraph" via
[pip](https://pypi.org/project/pip/) from
[PyPI](https://pypi.org/project):

    $ pip install pytest-callgraph

## Usage

-   TODO

## Contributing

Contributions are very welcome. Tests can be run with
[tox](https://tox.readthedocs.io/en/latest/), please ensure the coverage
at least stays the same before you submit a pull request.

## License

Distributed under the terms of the [Apache Software License
2.0](http://www.apache.org/licenses/LICENSE-2.0) license,
\"pytest-callgraph\" is free and open source software

## Issues

If you encounter any problems, please [file an
issue](https://github.com/mindriot101/pytest-callgraph/issues) along
with a detailed description.
